//https://github.com/request/request-promise

var cardData, cardActionsData, cardAttachmentsData, cardBoardData, cardCheckItemStatesData, cardChecklistsData, cardListData, cardMembersData, cardMembersVotedData;
var cardId = '5acb5c9e85e41248f891d06d'

if (process.env.NODE_ENV != 'production') {
  require('dotenv').config()
}


trelloGetCardInfo(cardId)

var verifiedTimes = 1;
var refreshIntervalId = setInterval(isObjectsReady, 1000);

function isObjectsReady() {
  isTrelloReady = isTrelloObjectsFilled()
  if (verifiedTimes == 15 || isTrelloReady) {
    if (isTrelloReady) {
      // console.log('Ready: Let\'s go')
      // console.log(`cardData length: ${cardData.length}`)
      // console.log(`cardActionsData length: ${cardActionsData.length}`)
      // console.log(`cardAttachmentsData length: ${cardAttachmentsData.length}`)
      // console.log(`cardBoardData length: ${cardBoardData.length}`)
      // console.log(`cardCheckItemStatesData length: ${cardCheckItemStatesData.length}`)
      // console.log(`cardChecklistsData length: ${cardChecklistsData.length}`)
      // console.log(`cardListData length: ${cardListData.length}`)
      // console.log(`cardMembersData length: ${cardMembersData.length}`)
      // console.log(`cardMembersVotedData length: ${cardMembersVotedData.length}`)

      airtableIntegration(cardData, cardActionsData, cardAttachmentsData, cardBoardData, cardCheckItemStatesData, cardChecklistsData, cardListData, cardMembersData)
    }
    clearInterval(refreshIntervalId);
  }
  // console.log(`executado ${verifiedTimes}x`);
  verifiedTimes++;
}

function airtableIntegration(cardData, actionsData, attachmentsData, boardData, checkItemStatesData, checklistsData, listData, membersData) {
  // console.log('airtableIntegration')
  var where = require("lodash.where");
  var find = require('lodash.find');

  var card = JSON.parse(cardData)
  var actions = JSON.parse(actionsData)
  var attachments = attachmentsData !== undefined ? JSON.parse(attachmentsData) : ''
  var board = JSON.parse(boardData)
  var checkItemStates = checkItemStatesData !== undefined ? JSON.parse(checkItemStatesData) : ''
  var checklists = JSON.parse(checklistsData)
  var list = JSON.parse(listData)
  var members = membersData !== undefined ? JSON.parse(membersData) : ''

  console.log(membersData)
  console.log(members)

  // console.log(card)
  // console.log(Object.keys(checklists.checkItems))


  // console.log(`id list: ${card.idList}`)
  // console.log(`idMembers: ${card.idMembers.join(',')}`)

  // var persons = where(members, {id: card.idMembers});
  // var persons = where(members, function(m) { return m.username; })

  // var persons = where(members, function(item) {
  //    return item.id  === card.idMembers.join(',');
  // });

  // console.log(`taskCount: ${taskCount}`);

  // var persons = find(members, { id: card.idMembers.join(',') });
  // console.log(persons)
  // console.log(members.where)
  // console.log()
  // console.log(card.name)
  // console.log(actions)
  // console.log(attachments.length)
  // console.log(board.name)
  // console.log(checklists.length) // qtd de checklists
  // if (checklists.length > 0) {
  //   var total_tasks = checklists[0].checkItems.length
  //   var pending_tasks = total_tasks - completed_tasks
  //   // console.log(`quantidade de tarefas: ${total_tasks}`)
  //   // console.log(`tarefas pendentes: ${pending_tasks}`)
  // }
  // console.log(`etapa trello: ${list.name}`)


  var trello_list = list.name
  var trello_link = card.shortUrl
  var type = '???'
  var completed_tasks = checkItemStates.length;
  var total_tasks = 0;
  for (var c in checklists) {
    total_tasks += checklists[c].checkItems.length
  }

  // console.log(`etapa trello: ${trello_list}`)
  // console.log(`link trello: ${trello_link}`)
  // console.log(`tipo: ${type == '???' ? 'Setup' : type}`)
  // console.log(`projeto: ${card.name}`)
  console.log(`projetos: ${type}`)
  console.log(`responsável: ${type}`) /// members.username separados por virgula
  console.log(`próxima atividade: ${type}`) //cyustom field
  // console.log(`due date: ${card.due}`)
  // console.log(`total task pendentes: ${total_tasks - completed_tasks}`)
  // console.log(`tasks completas: ${completed_tasks}`)
  // console.log(`data ultima atualização: ${card.dateLastActivity}`)

  // console.log('integration complete')
}


function isTrelloObjectsFilled() {
  var exists = (cardData !== undefined && cardActionsData !== undefined && cardBoardData != undefined && cardListData !== undefined)
  if (exists) {
    // verifica se os campos estão preenchidos para então executar a chamada da api do airtable
    // para atualizar ou criar novos registros
    return (cardData.length && cardActionsData.length && cardBoardData.length && cardListData.length);
  }
  return exists;
}

function trelloGetCardInfo(cardId) {
  getCard(cardId)
  getCardActions(cardId)
  getCardAttachments(cardId)
  getCardBoard(cardId)
  getCardCheckItemStates(cardId)
  getCardChecklists(cardId)
  getCardList(cardId)
  getCardMembers(cardId)
  // getCardMembersVoted(cardId) // not required yet
}

function requestOptions(card_id, content) {
  options = {
    method: 'GET',
    url: `https://api.trello.com/1/cards/${card_id}`,
    qs: { key: process.env.TRELLO_API_KEY, token: process.env.TRELLO_TOKEN }
  };

  if (content && content.length) {
    options.url += '/' + content
  }
  return options;
}

function getCard(id) {
  // console.log('getCard')
  var request = require('request')
  request(requestOptions(id), function(error, response, body) {
    if (error) throw new Error(error);
    cardData = body;
  });
}

function getCardActions(id) {
  // console.log('getCardActions')
  var request = require('request')
  request(requestOptions(id, 'actions'), function(error, response, body) {
    if (error) throw new Error(error);
    cardActionsData = body;

    // console.log(cardActionsData)
  });
}

function getCardAttachments(id) {
  // console.log('getCardAttachments')
  var request = require('request')
  request(requestOptions(id, 'attachments'), function(error, response, body) {
    if (error) throw new Error(error);
    cardAttachmentsData = body;
  });
}

function getCardBoard(id) {
  // console.log('getCardBoard')
  var request = require('request')
  request(requestOptions(id, 'board'), function(error, response, body) {
    if (error) throw new Error(error);
    cardBoardData = body;
  });
}

function getCardCheckItemStates(id) {
  // console.log('getCardCheckItemStates')
  var request = require('request')
  request(requestOptions(id, 'checkItemStates'), function(error, response, body) {
    if (error) throw new Error(error);
    cardCheckItemStatesData = body;
  });
}

function getCardChecklists(id) {
  // console.log('getCardChecklists')
  var request = require('request')
  request(requestOptions(id, 'checklists'), function(error, response, body) {
    if (error) throw new Error(error);
    cardChecklistsData = body;
  });
}

function getCardList(id) {
  // console.log('getCardList')
  var request = require('request')
  request(requestOptions(id, 'list'), function(error, response, body) {
    if (error) throw new Error(error);
    cardListData = body;
  });
}

function getCardMembers(id) {
  // console.log('getCardMembers')
  var request = require('request')
  request(requestOptions(id, 'members'), function(error, response, body) {
    if (error) throw new Error(error);
    cardMembersData = body;
  });
}

function getCardMembersVoted(id) {
  // console.log('getCardMembersVoted')
  var request = require('request')
  request(requestOptions(id, 'membersVoted'), function(error, response, body) {
    if (error) throw new Error(error);
    cardMembersVotedData = body;
  });
}
